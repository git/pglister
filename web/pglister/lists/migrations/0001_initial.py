# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
from django.core.validators import RegexValidator


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArchiveServer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('urlpattern', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Domain',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('lists_helppage', models.URLField(max_length=100)),
                ('archiveservers', models.ManyToManyField(to='lists.ArchiveServer', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='GlobalWhitelist',
            fields=[
                ('address', models.EmailField(max_length=254, serialize=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='List',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=False, blank=False, validators=[RegexValidator(r"^[a-zA-Z0-9-]+$", "List name can only contain alphanumerical characters and hypens.")])),
                ('subscription_policy', models.IntegerField(choices=[(0, 'Anybody can subscribe'), (1, 'Requires moderator approval'), (2, 'Moderator managed, unlisted')])),
                ('moderation_level', models.IntegerField(choices=[(0, 'No moderation'), (1, 'Direct subscribers can post'), (2, 'Global confirmed addresses can post'), (3, 'All emails are moderated'), (4, 'All emails are double-moderated')])),
                ('spamscore_threshold', models.DecimalField(null=True, max_digits=5, decimal_places=1, blank=True)),
                ('archivedat', models.ForeignKey(blank=True, to='lists.ArchiveServer', null=True, on_delete=models.CASCADE)),
                ('domain', models.ForeignKey(to='lists.Domain', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='ListGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('groupname', models.CharField(max_length=100)),
                ('sortkey', models.IntegerField(default=10)),
            ],
            options={'ordering': ('sortkey',)},
        ),
        migrations.CreateModel(
            name='ListSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subscription_confirmed', models.BooleanField(default=True)),
                ('nomail', models.BooleanField(default=False)),
                ('list', models.ForeignKey(to='lists.List', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='ListSubscriptionModerationToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tokensent', models.DateTimeField()),
                ('token', models.TextField(unique=True)),
                ('subscription', models.ForeignKey(to='lists.ListSubscription', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='ListWhitelist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.EmailField(max_length=254)),
                ('list', models.ForeignKey(to='lists.List', on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('user', models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={'ordering': ('user__username',)},
        ),
        migrations.CreateModel(
            name='SubscriberAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('confirmed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='SubscriberAddressToken',
            fields=[
                ('subscriberaddress', models.OneToOneField(primary_key=True, serialize=False, to='lists.SubscriberAddress', on_delete=models.CASCADE)),
                ('tokensent', models.DateTimeField(auto_now_add=True)),
                ('token', models.TextField(unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='subscriberaddress',
            name='subscriber',
            field=models.ForeignKey(blank=True, to='lists.Subscriber', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='listsubscription',
            name='subscriber',
            field=models.ForeignKey(to='lists.SubscriberAddress', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='list',
            name='group',
            field=models.ForeignKey(to='lists.ListGroup', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='list',
            name='moderators',
            field=models.ManyToManyField(to='lists.Subscriber', blank=False, null=False, related_name='moderator_of_lists'),
        ),
        migrations.AddField(
            model_name='list',
            name='subscribers',
            field=models.ManyToManyField(to='lists.SubscriberAddress', through='lists.ListSubscription', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='listwhitelist',
            unique_together=set([('list', 'address')]),
        ),
    ]
