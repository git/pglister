# Generated by Django 3.2.6 on 2021-11-16 21:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0054_add_archivedat'),
    ]

    run_before = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.RunSQL("DROP VIEW mailinglist_moderators"),
    ]
