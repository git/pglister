# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0005_list_descriptions'),
    ]

    operations = [
        migrations.AlterField(
            model_name='listsubscriptionmoderationtoken',
            name='subscription',
            field=models.OneToOneField(to='lists.ListSubscription', on_delete=models.CASCADE),
        ),
    ]
