from django.db import models


class Level():
    INFO = 0
    WARNING = 1
    ERROR = 2


levelstrings = [
    'Info',
    'Warning',
    'Error',
]


class LogEntry(models.Model):
    t = models.DateTimeField(null=False, blank=False, db_index=True, verbose_name='Timestamp')
    level = models.IntegerField(null=False, blank=False, default=0)
    source = models.CharField(null=False, blank=False, max_length=10)
    confirmed = models.BooleanField(null=False, blank=False, default=False)
    username = models.CharField(null=True, blank=True, max_length=30)
    messageid = models.CharField(max_length=1000, null=True, blank=True)
    msg = models.CharField(max_length=1000, null=False, blank=False)

    @property
    def levelstr(self):
        return levelstrings[self.level]

    class Meta:
        managed = False
        db_table = 'log'
        ordering = ('-t', )
