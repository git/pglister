from django.db import connection
from django.conf import settings

from datetime import datetime, timedelta


def maybe_refresh_exim_queue():
    if not settings.EXIM_INTEGRATION:
        return

    # Import the model later, since this app is not installed if exim integration isn't
    # enabled.
    from .models import RefreshDate

    try:
        rd = RefreshDate.objects.get(viewname='queue')
    except RefreshDate.DoesNotExist:
        rd = RefreshDate(viewname='queue', lastrefresh=datetime.now() - timedelta(weeks=1))

    # Refresh if data is older than 5 minutes
    if datetime.now() - rd.lastrefresh > timedelta(minutes=5):
        connection.cursor().execute("REFRESH MATERIALIZED VIEW CONCURRENTLY eximintegration.queue")
        rd.lastrefresh = datetime.now()
        rd.save()

    return rd.lastrefresh
