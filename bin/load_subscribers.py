#!/usr/bin/env python3
#
# load_subscribers.py - load a list of subscribers into a list
#
#
# This script is intended for migrating subscribers from a different
# system, and *not* for incremental management of subscribers!
#
import os
import sys
import argparse

from email.utils import parseaddr

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))


from baselib.config import config

from baselib.misc import generate_random_token

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Load subscribers")
    parser.add_argument('-l', '--list', dest='list', help='Name of list to load into', required=True)
    parser.add_argument('-f', '--file', dest='filename', help='Name of file to load', required=True)
    parser.add_argument('-a', '--append', dest='append', help='Append to existing list', action='store_true')
    parser.add_argument('--mj2', action='store_true', help='Input is mj2 format list')
    parser.add_argument('-n', '--nomail', action='store_true', help='Add with mail delivery disabled')

    args = parser.parse_args()

    conn = config.conn('load_subscribers')
    curs = conn.cursor()

    with open(args.filename) as f:
        if not args.mj2:
            subscribers = [(parseaddr(r.strip())[1].lower(), False) for r in f.readlines() if r != "\n"]
        else:
            # Mj2 parsing is ugly...
            mj2info = [l.strip().lower().rsplit(None, 2) for l in f.readlines() if l.startswith(' ')]
            subscribers = [(parseaddr(m[0])[1], 'nomail' in m[-1]) for m in mj2info]

    knownbroken = [a for a, b in subscribers if a.find('@') < 0]
    knownbroken.extend([a for a, b in subscribers if a.find(' ') > -1])
    if knownbroken:
        print("Found broken addresses:")
        print("\n".join(knownbroken))
        print("NOT loading")
        sys.exit(1)

    curs.execute("BEGIN TRANSACTION")
    curs.execute("SELECT id FROM mailinglists WHERE address=%(addr)s", {
        'addr': args.list,
    })
    r = curs.fetchall()
    if len(r) != 1:
        print("Could not find list %s. Did you specify the full address?" % args.list)
        sys.exit(1)

    listid = r[0][0]

    curs.execute("SELECT count(*) from lists_listsubscription WHERE list_id=%(listid)s", {
        'listid': listid,
    })
    if curs.fetchall()[0][0] > 0 and not args.append:
        print("List already contains subscribers!")
        print("If you want to append to existing subscribers, use the -a flag")
        conn.rollback()
        sys.exit(1)

    while True:
        r = input('Found %s entries in file. Start loading? ' % len(subscribers)).strip().lower()
        if r == 'y':
            break
        elif r == 'n':
            print("Ok, aborting")
            conn.rollback()
            sys.exit(1)

    newaddr = 0
    newsub = 0
    for s, nomail in subscribers:
        curs.execute("SELECT id FROM lists_subscriberaddress WHERE email=%(email)s", {
            'email': s,
        })
        r = curs.fetchall()
        if len(r) == 0:
            curs.execute("INSERT INTO lists_subscriberaddress (email, confirmed, blocked, token) VALUES (%(email)s, 't', 'f', %(token)s) RETURNING id", {
                'email': s,
                'token': generate_random_token(),
            })
            r = curs.fetchall()
            newaddr += 1
        if len(r) != 1:
            print("Invalid number of rows for loading subscriberaddress for %s" % s)
            conn.rollback()
            sys.exit(1)

        # Also add the subscriber to the list
        curs.execute("INSERT INTO lists_listsubscription(nomail, list_id, subscriber_id, subscription_confirmed) VALUES (%(nomail)s, %(listid)s, %(subid)s, 't') {0}".format(args.append and " ON CONFLICT DO NOTHING" or ""), {
            'listid': listid,
            'subid': r[0][0],
            'nomail': nomail or args.nomail,
        })
        newsub += curs.rowcount

    if newaddr > 0:
        while True:
            r = input('Added %s subscribers, of which %s were new. Commit? ' % (newsub, newaddr)).strip().lower()
            if r == 'y':
                break
            elif r == 'n':
                print("OK, aborting")
                conn.rollback()
                sys.exit(1)
        print("OK, committing")
        conn.commit()
    else:
        print("No new subscriber, nothing to do.")
    print("Done.")
    conn.close()
