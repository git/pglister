#!/usr/bin/python3 -u
#
# eximcleaner.py - clean up emails in the exim queue that are for unsubscribed users or
#                  expired moderation notices.
#
# This script is intended to be run as a daemon. This daemon must have permissions to
# process the exim mailqueue both for read and write!
#


import os
import sys
import select
import time

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from baselib.config import config
from handlers.eximcleaner import EximCleanerHandler

if __name__ == "__main__":
    conn = config.conn('eximcleaner')
    curs = conn.cursor()

    curs.execute("LISTEN eximclean")
    conn.commit()

    while True:
        # Process all pending clean requests if any
        EximCleanerHandler(conn).process()

        # Wait for activity for up to 15 minutes. If nothing happened,
        # then we run the process *anyway*, just in case we missed a
        # notify somewhere. This is not a time urgent task so we only
        # need to check every 15 minutes or so.
        select.select([conn], [], [], 15 * 60)

        # Eat up all notifications, since we're just going to process
        # all pending messages until the queue is empty.
        conn.poll()
        while conn.notifies:
            conn.notifies.pop()

        # Sleep for a bit before we process the data, so we don't end up
        # running the mailq command way too often.
        time.sleep(5)

        # Loop back up and process the full queue
