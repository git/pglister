#!/usr/bin/env python3
#
# Convert the exim mailqueue to json
#

import json
import subprocess

if __name__ == "__main__":
    q = []
    p = subprocess.run(['/usr/bin/mailq', ], check=True, capture_output=True, text=True)

    current = {}
    for l in p.stdout.splitlines():
        if l == '':
            if current and current.get('eximid', None):
                if not current.get('frozen', False):
                    q.append(current)
                current = {}
            continue

        if '*** frozen ***' in l:
            # If the mail is frozen, we ignore the whole thing
            current = {'frozen': True, 'recipients': []}
            continue

        pieces = l.split()
        if len(pieces) == 4:
            # First line of a message
            current['time'] = pieces[0]
            current['size'] = pieces[1]
            current['eximid'] = pieces[2]
            current['sender'] = pieces[3].strip("<>")
            current['recipients'] = []
        elif len(pieces) == 1 and current:
            current['recipients'].append({'address': pieces[0], 'sent': False})
        elif len(pieces) == 2 and current and pieces[0] == 'D':
            # Already delivered
            current['recipients'].append({'address': pieces[1], 'sent': True})
        else:
            raise Exception("Unparseable line: {}".format(l))

    for m in q:
        print(json.dumps(m))
